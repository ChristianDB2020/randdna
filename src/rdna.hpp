#include <string>
#include <random>
#include <iostream>


using std::string;
using namespace std;

string randDNA(int seed, string bases, int n)
{
	string rDNA = "";
	int min = 0;
	int max = 0;
	mt19937 eng(seed);
	
	if (bases == ""){
		return "";
	}
	else{
		max = bases.size() - 1;
	}
	
	uniform_int_distribution<> un(min,max);
	
	for(int i = 0; i <= max; i++){
		bases[i];
		cout << "The base:  ["<< i <<"] = " + bases[i] << endl;
	}
	
	int index = 0;
	
	for(int y = 0; y < n; y++){
		index = un(eng);
		cout << "Random Output is: " + index << endl;
		rDNA += bases[index];
	}
	
	return rDNA;
	
}
